%global check_password_name openldap-ppolicy-check-password
%global check_password_version 1.1
%global so_ver 2
%global so_ver_compat 2
%global __brp_remove_la_files %nil

Summary: An open source implementation of the LDAP
Name:    openldap
Version: 2.6.5
Release: 4%{?dist}
License: OpenLDAP Public License
URL:     https://www.openldap.org/
Source0: https://openldap.org/software/download/OpenLDAP/openldap-release/%{name}-%{version}.tgz
Source1: https://github.com/ltb-project/%{check_password_name}/archive/v%{check_password_version}/%{check_password_name}-%{check_password_version}.tar.gz
Source2: slapd.service
Source3: slapd.ldif
Source4: ldap.conf
Source5: libexec-functions
Source6: libexec-check-config.sh
Source7: check_password.conf

Patch3000: openldap-manpages.patch
Patch3001: openldap-reentrant-gethostby.patch
Patch3002: openldap-smbk5pwd-overlay.patch
Patch3003: openldap-ai-addrconfig.patch
Patch3004: openldap-allop-overlay.patch
Patch3005: openldap-fix-missing-mapping.patch
Patch3006: check-password-makefile.patch
Patch3007: check-password.patch

BuildRequires: cyrus-sasl-devel gcc glibc-devel groff krb5-devel unixODBC-devel
BuildRequires: libevent-devel make openssl-devel libtool-ltdl-devel
BuildRequires: perl(ExtUtils::Embed) perl-devel perl-generators perl-interpreter

%description
OpenLADP is a suite of the Lightweight Directory Access Protocol (v3) 
servers, clients, utilities, and development tools.

%package servers
Summary: A stand-alone LDAP directory server
BuildRequires: systemd cracklib-devel
Requires: openldap = %{version}-%{release}
Requires(pre): shadow-utils
%{?systemd_requires}
Provides: ldif2ldbm

%description servers
This package contains the slapd server and related files.

%package clients
Summary: LDAP client utilities
Requires: openldap = %{version}-%{release}

%description clients
The package contains the client programs needed for accessing
and modifying OpenLDAP directories.

%package devel
Summary: LDAP development libraries and header files
Requires: openldap = %{version}-%{release}
Requires: cyrus-sasl-devel

%description devel
The package contains the development libraries and header files 
needed for compiling applications that use Lightweight Directory 
Access Protocol (v3) internals. 

%package compat
Summary: Package providing legacy non-threaded libldap
Requires: openldap = %{version}-%{release}
Provides: libldap-2.4.so.%{so_ver_compat}
Provides: libldap_r-2.4.so.%{so_ver_compat}
Provides: liblber-2.4.so.%{so_ver_compat}
Provides: libslapi-2.4.so.%{so_ver_compat}

%description compat
The package contains shared libraries available for compatibility reasons.

%prep
%setup -q -c -a 1

pushd %{name}-%{version}
%autopatch -m 3000 -M 3005 -p1
mv contrib/slapd-modules/smbk5pwd/smbk5pwd.c servers/slapd/overlays/
mv contrib/slapd-modules/smbk5pwd/README{,.smbk5pwd}
mv contrib/slapd-modules/smbk5pwd/slapo-smbk5pwd.5 doc/man/man5/

mv contrib/slapd-modules/allop/allop.c servers/slapd/overlays
mv contrib/slapd-modules/allop/README{,.allop}
mv contrib/slapd-modules/allop/slapo-allop.5 doc/man/man5/

mv servers/slapd/back-perl/README{,.back_perl}

# Convert encoding from iso-8859-1 to utf-8
filename=doc/drafts/draft-ietf-ldapext-acl-model-xx.txt
iconv -f iso-8859-1 -t utf-8 -o "$filename.utf8" "$filename"
mv -f "$filename.utf8" "$filename"
popd

pushd %{check_password_name}-%{check_password_version}
%patch3006 -p1
%patch3007 -p1
popd

%build
export CFLAGS="${CFLAGS} ${LDFLAGS} -Wl,--as-needed -DLDAP_CONNECTIONLESS"

pushd %{name}-%{version}
%configure \
	--enable-debug \
	--enable-dynamic \
	--enable-versioning \
	--enable-dynacl \
	--enable-cleartext \
	--enable-crypt \
	--enable-lmpasswd \
	--enable-spasswd \
	--enable-modules \
	--enable-perl \
	--enable-rewrite \
	--enable-rlookups \
	--enable-slapi \
	--enable-bdb=yes \
	--enable-hdb=yes \
	--enable-mdb=yes \
	--enable-monitor=yes \
	--enable-backends=mod \
	--enable-overlays=mod \
	--enable-balancer=mod \
	--disable-slp \
	--disable-ndb \
	--disable-sql \
	--disable-wt \
	--disable-static \
	--with-cyrus-sasl \
	--without-fetch \
	--with-threads \
	--with-pic \
	--with-gnu-ld \
	--libexecdir=%{_libdir}

%make_build
popd

pushd %{check_password_name}-%{check_password_version}
%make_build LDAP_INC="-I../%{name}-%{version}/include -I../%{name}-%{version}/servers/slapd -I../%{name}-%{version}/build-servers/include"
popd

%install
mkdir -p %{buildroot}%{_libdir}/

pushd %{name}-%{version}
%make_install STRIP=""
popd

pushd %{check_password_name}-%{check_password_version}
mv check_password.so check_password.so.%{check_password_version}
ln -s check_password.so.%{check_password_version} %{buildroot}%{_libdir}/openldap/check_password.so
install -m 755 check_password.so.%{check_password_version} %{buildroot}%{_libdir}/openldap/
install -d -m 755 %{buildroot}%{_sysconfdir}/openldap
install -m 644 %SOURCE7 %{buildroot}%{_sysconfdir}/openldap/check_password.conf
mv README{,.check_pwd}
popd

mkdir -p %{buildroot}%{_sysconfdir}/openldap/certs
mkdir -p %{buildroot}%{_sharedstatedir}
mkdir -p %{buildroot}%{_localstatedir}
install -m 0700 -d %{buildroot}%{_sharedstatedir}/ldap
install -m 0755 -d %{buildroot}%{_localstatedir}/run/openldap

rm %{buildroot}%{_sysconfdir}/openldap/ldap.conf
install -m 0644 %SOURCE4 %{buildroot}%{_sysconfdir}/openldap/ldap.conf

mkdir -p %{buildroot}%{_libexecdir}
install -m 0755 -d %{buildroot}%{_libexecdir}/openldap
install -m 0644 %SOURCE5 %{buildroot}%{_libexecdir}/openldap/functions
install -m 0755 %SOURCE6 %{buildroot}%{_libexecdir}/openldap/check-config.sh

rm %{buildroot}%{_sysconfdir}/openldap/*.default

mkdir -p %{buildroot}%{_unitdir}
install -m 0644 %SOURCE2 %{buildroot}%{_unitdir}/slapd.service

for applet in $(ls %{buildroot}%{_sbindir}); do
    rm %{buildroot}%{_sbindir}/${applet}
    ln -s slapd %{buildroot}%{_sbindir}/${applet}
done
mv %{buildroot}%{_libdir}/slapd %{buildroot}%{_sbindir}/

pushd %{buildroot}%{_libdir}
v=%{version}
version=$(echo ${v%.[0-9]*})
for lib in liblber libldap libslapi; do
        rm -f ${lib}.so
        ln -s ${lib}.so.%{so_ver} ${lib}.so
done

for lib in $(ls | grep libldap); do
    IFS='.'
    read -r -a libsplit <<< "$lib"
    if [[ -z "${libsplit[3]}" && -n "${libsplit[2]}" ]]
    then
        so_ver_short_2_4="%{so_ver_compat}"
    elif [ -n "${libsplit[3]}" ]
    then
        so_ver_full_2_4="%{so_ver_compat}.${libsplit[3]}.${libsplit[4]}"
    fi
    unset IFS
done

gcc -shared -o "%{buildroot}%{_libdir}/libldap-2.4.so.${so_ver_short_2_4}" -Wl,--no-as-needed \
       -Wl,-soname -Wl,libldap-2.4.so.${so_ver_short_2_4} -L "%{buildroot}%{_libdir}" -lldap
gcc -shared -o "%{buildroot}%{_libdir}/libldap_r-2.4.so.${so_ver_short_2_4}" -Wl,--no-as-needed \
       -Wl,-soname -Wl,libldap_r-2.4.so.${so_ver_short_2_4} -L "%{buildroot}%{_libdir}" -lldap
gcc -shared -o "%{buildroot}%{_libdir}/liblber-2.4.so.${so_ver_short_2_4}" -Wl,--no-as-needed \
       -Wl,-soname -Wl,liblber-2.4.so.${so_ver_short_2_4} -L "%{buildroot}%{_libdir}" -llber
gcc -shared -o "%{buildroot}%{_libdir}/libslapi-2.4.so.${so_ver_short_2_4}" -Wl,--no-as-needed \
       -Wl,-soname -Wl,libslapi-2.4.so.${so_ver_short_2_4} -L "%{buildroot}%{_libdir}" -lslapi
ln -s libldap-2.4.so.{${so_ver_short_2_4},${so_ver_full_2_4}}
ln -s libldap_r-2.4.so.{${so_ver_short_2_4},${so_ver_full_2_4}}
ln -s liblber-2.4.so.{${so_ver_short_2_4},${so_ver_full_2_4}}
ln -s libslapi-2.4.so.{${so_ver_short_2_4},${so_ver_full_2_4}}
popd

chmod 0755 %{buildroot}%{_libdir}/lib*.so*
chmod 0644 %{buildroot}%{_libdir}/lib*.*a
chmod 0644 %{buildroot}%{_libdir}/openldap/*.la

mkdir -p %{buildroot}%{_datadir}
install -m 0755 -d %{buildroot}%{_datadir}/openldap-servers
install -m 0644 %SOURCE3 %{buildroot}%{_datadir}/openldap-servers/slapd.ldif
install -m 0700 -d %{buildroot}%{_sysconfdir}/openldap/slapd.d
rm %{buildroot}%{_sysconfdir}/openldap/slapd.conf
rm %{buildroot}%{_sysconfdir}/openldap/slapd.ldif

mv %{buildroot}%{_sysconfdir}/openldap/schema/README README.schema
rm %{buildroot}%{_libdir}/*.la  

%pre servers
getent group ldap &>/dev/null || groupadd -r -g 55 ldap
getent passwd ldap &>/dev/null || \
	useradd -r -g ldap -u 55 -d %{_sharedstatedir}/ldap -s /sbin/nologin -c "OpenLDAP server" ldap
exit 0

%post servers
%systemd_post slapd.service

if [[ ! -f %{_sysconfdir}/openldap/slapd.d/cn=config.ldif && \
      ! -f %{_sysconfdir}/openldap/slapd.conf
   ]]; then
      mkdir -p %{_sysconfdir}/openldap/slapd.d/ &>/dev/null || :
      /usr/sbin/slapadd -F %{_sysconfdir}/openldap/slapd.d/ -n0 -l %{_datadir}/openldap-servers/slapd.ldif
      chown -R ldap:ldap %{_sysconfdir}/openldap/slapd.d/
      /usr/bin/systemctl try-restart slapd.service &>/dev/null
fi

if [ $1 -ge 1 ]; then
    /usr/bin/systemctl condrestart slapd.service &>/dev/null || :
fi
exit 0

%preun servers
%systemd_preun slapd.service

%postun servers
%systemd_postun_with_restart slapd.service

%files
%license %{name}-%{version}/{COPYRIGHT,LICENSE}
%doc %{name}-%{version}/{ANNOUNCEMENT,CHANGES,README}
%dir %{_sysconfdir}/openldap
%dir %{_sysconfdir}/openldap/certs
%config(noreplace) %{_sysconfdir}/openldap/ldap.conf
%dir %{_libexecdir}/openldap/
%{_libdir}/liblber.so.*
%{_libdir}/libldap.so.*
%{_libdir}/libslapi.so.*
%{_mandir}/man5/ldif.5*
%{_mandir}/man5/ldap.conf.5*

%files servers
%doc %{name}-%{version}/contrib/slapd-modules/smbk5pwd/README.smbk5pwd
%doc %{name}-%{version}/doc/guide/admin/{guide.html,*.png}
%doc %{name}-%{version}/servers/slapd/back-perl/{SampleLDAP.pm,README.back_perl}
%doc %{check_password_name}-%{check_password_version}/README.check_pwd README.schema
%config(noreplace) %dir %attr(0750,ldap,ldap) %{_sysconfdir}/openldap/slapd.d
%ghost %config(noreplace,missingok) %attr(0640,ldap,ldap) %{_sysconfdir}/openldap/slapd.conf
%config(noreplace) %{_sysconfdir}/openldap/schema
%config(noreplace) %{_sysconfdir}/openldap/check_password.conf
%dir %attr(0700,ldap,ldap) %{_sharedstatedir}/ldap
%dir %attr(-,ldap,ldap) %{_localstatedir}/run/openldap
%{_unitdir}/slapd.service
%{_datadir}/openldap-servers/
%{_libdir}/openldap/accesslog*
%{_libdir}/openldap/allop*
%{_libdir}/openldap/auditlog*
%{_libdir}/openldap/autoca*
%{_libdir}/openldap/back_asyncmeta*
%{_libdir}/openldap/back_dnssrv*
%{_libdir}/openldap/back_ldap*
%{_libdir}/openldap/back_meta*
%{_libdir}/openldap/back_null*
%{_libdir}/openldap/back_passwd*
%{_libdir}/openldap/back_relay*
%{_libdir}/openldap/back_sock*
%{_libdir}/openldap/check_password*
%{_libdir}/openldap/collect*
%{_libdir}/openldap/constraint*
%{_libdir}/openldap/dds*
%{_libdir}/openldap/deref*
%{_libdir}/openldap/dyngroup*
%{_libdir}/openldap/dynlist*
%{_libdir}/openldap/home*
%{_libdir}/openldap/lloadd*
%{_libdir}/openldap/memberof*
%{_libdir}/openldap/otp*
%{_libdir}/openldap/pcache*
%{_libdir}/openldap/ppolicy*
%{_libdir}/openldap/refint*
%{_libdir}/openldap/remoteauth*
%{_libdir}/openldap/retcode*
%{_libdir}/openldap/rwm*
%{_libdir}/openldap/seqmod*
%{_libdir}/openldap/smbk5pwd*
%{_libdir}/openldap/sssvlv*
%{_libdir}/openldap/syncprov*
%{_libdir}/openldap/translucent*
%{_libdir}/openldap/unique*
%{_libdir}/openldap/valsort*
%{_libexecdir}/openldap/functions
%{_libexecdir}/openldap/check-config.sh
%{_sbindir}/slap*
%{_mandir}/man8/*
%{_mandir}/man5/lloadd.conf.5*
%{_mandir}/man5/slapd*.5*
%{_mandir}/man5/slapo-*.5*
%{_mandir}/man5/slappw-argon2.5*

%files clients
%{_bindir}/*
%{_mandir}/man1/*

%files devel
%doc %{name}-%{version}/doc/{drafts,rfc}
%{_includedir}/*
%{_libdir}/pkgconfig/lber.pc
%{_libdir}/pkgconfig/ldap.pc
%{_libdir}/liblber.so
%{_libdir}/libldap.so
%{_libdir}/libslapi.so
%{_mandir}/man3/*

%files compat
%{_libdir}/libldap-2.4*.so.*
%{_libdir}/libldap_r-2.4*.so.*
%{_libdir}/liblber-2.4*.so.*
%{_libdir}/libslapi-2.4*.so.*

%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.6.5-4
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.6.5-3
- Rebuilt for loongarch release

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.6.5-2
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Jul 14 2023 Xiaojie Chen <jackxjchen@tencent.com> - 2.6.5-1
- Upgrade to upstream version 2.6.5

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.6.2-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.6.2-2
- Rebuilt for OpenCloudOS Stream 23

* Fri Jun 10 2022 Xiaojie Chen <jackxjchen@tencent.com> - 2.6.2-1
- Initial build
